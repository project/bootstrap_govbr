This is a [Barrio](https://www.drupal.org/project/bootstrap_barrio "Bootstrap Barrio") subtheme that simplifies integrating Bootstrap 4 SASS, GovBR visual standards, and "Barra do Governo" with Drupal.

This subtheme overrides almost every CSS from Drupal and replaces where posible Bootstrap variables in order to generate from roots a new set of CSS files.

Color management relies on Google Material Design, generating lighten and darken variations of main color to generate hover or contrast for main colors.

For variables overide a SCSS file is defined, changinmg the whole Bootstrap definition according to personalizations set.

This subtrheme is also compatible with Material Desingn Bootstrap.

For a deeper understanding ir the theme options:

[SASS Subtheme Configuration Guide](https://www.drupal.org/docs/8/themes/bootstrap-4-sass-barrio-starter-kit-0 "Bootstrap Barrio SASS Guide")

Installation
------------

*   Install node.js on your server
*   On the theme´s folder:
*   Install gulp: `npm install --global gulp-cli`
*   Install dependencies including Bootstrap latest version: `npm install`
*   Optional install Material Design Bootstrap: `npm install mdbootstrap`

Update line#47 of the gulpfile.js file with your own domain.

    browserSync.init({
      proxy: "http://yourdomain.com",
    });

This will generate a style.css file with mappings for debbuging and a style.min.css file for production. You will need to change the reference to the file manually on your SUBTHEME.libraries.yml file.

Execution
---------

Just run `gulp`
